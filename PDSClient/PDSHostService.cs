﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using Microsoft.Samples.XmlRpc;
using PDSService;
using System.Threading.Tasks;
using Agrawala;
using TokenRing;
using System.ServiceModel.Description;

namespace PDSServiceHost
{
    class PDSHostService
    {
 
        private static ServiceHost serviceHost;
        private static int synchAlgType = 0;
        static void Main(string[] args)
        {
            synchAlgType = 0;
            //readAlgorithmType();
            initHost();
            Console.WriteLine("The service is up and running.");
            Console.WriteLine("Press <ENTER> to terminate service.");
            Console.ReadLine();

            // Close the ServiceHostBase to shutdown the service.
            serviceHost.Close();
        }

        private static void readAlgorithmType()
        {
            string line;
            System.Console.WriteLine("Please specify the synchronization algorithm that you would like to use: 0 for Token Ring, 1 for Ricart & Agrawala");
            while ((line = Console.ReadLine()) != null)
            {
                if (line.Equals("0") || line.Equals("1"))
                {
                    synchAlgType = int.Parse(line);
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid parameter, please enter 0 or 1");
                    continue;
                }
            }
        }
      
        public static void initHost()
        {
            // Step 1 Create a URI to serve as the base address.
            string url = "http://127.0.0.1:8080/pds/";
            Uri baseAddress = new Uri(url);
            // Step 2 Create a ServiceHost instance
            ServiceEndpoint epXmlRpc = null;
            if (synchAlgType == 1)
            {
                serviceHost = new ServiceHost(typeof(ServerNodeA));
                // Step 3 Add a service endpoint.
                epXmlRpc = serviceHost.AddServiceEndpoint(typeof(INodeContractA),
                    new WebHttpBinding(WebHttpSecurityMode.None),
                    new Uri(baseAddress, ""));
            }
            else
            {
                serviceHost = new ServiceHost(typeof(ServerNodeTR));
                // Step 3 Add a service endpoint.
                 epXmlRpc = serviceHost.AddServiceEndpoint(typeof(INodeContractTR),
                    new WebHttpBinding(WebHttpSecurityMode.None),
                    new Uri(baseAddress, ""));
            }
            epXmlRpc.Behaviors.Add(new XmlRpcEndpointBehavior());
            serviceHost.Open();
            ChannelFactory<INodeContractA> cf;
            cf = new ChannelFactory<INodeContractA>(new WebHttpBinding(), url);

            cf.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());
        }
    }
}
