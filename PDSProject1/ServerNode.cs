﻿using Microsoft.Samples.XmlRpc;
using Agrawala;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
namespace PDSService
{
    // This is the implementation of the interface provided for the clients, this needs to reside 
    // only on the server side and is accessed by a client/remote nodes using rpc.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public abstract class ServerNode : INodeContract
    {
        // the address of this node
        public string NodeAddress { get; set; }
        public string UnformattedNodeAddress { get; set; }
        // the current value currently calculated on this node, note that this value has to be identical
        // in all the nodes
        public int CurrentValue { get; set; }
        // maintain a list of the addresses of the currently connected nodes in the network.
        protected SortedList<string, INodeContract> connectedNodes = new SortedList<string, INodeContract>();
        // total processing time during which all the nodes randomly perform some calculations
        protected int totalProcessTime = 20000; //time in ms

        protected Queue<OperationValuePair> operQueue = new Queue<OperationValuePair>();

        public ServerNode()
        {
            UnformattedNodeAddress = getMyIp();
            NodeAddress = formatUrl(UnformattedNodeAddress);
            createSyncAlgorithm();
            Console.WriteLine("my ip is: " + NodeAddress);
        }
        //
        public ServerNode(string nodeAddress, SortedList<string, INodeContract> connectedNodes)
        {
            this.NodeAddress = nodeAddress;
            this.connectedNodes = connectedNodes;
        }


        // this node wishes to join the network using nodeAddress, which is already an existing node 
        // in the network

        public virtual bool join(string url)
        {
            //TODO add validation: make sure that this node is really in the network
            Console.WriteLine("The node on this host wants to join a node on the network with ip:" + url);
            //string myNodeUrl = formatUrl(NodeAddress);
            string existingNodeUrl = formatUrl(url);
            INodeContract existingNetworkNode = createServerNode(url);

            //connectedNodes[url] = existingNetworkNode;
            try
            {
                string returnedNodesStr = existingNetworkNode.addNode(NodeAddress);
                string[] returnedNodes = returnedNodesStr.Split(',');
                Console.WriteLine("These network nodes were returned by the remote node:" + returnedNodesStr);
                foreach (string curNode in returnedNodes)
                {
                    connectedNodes[curNode] = createServerNode(curNode);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Exception occurred while joining the network: " + e.Message);
                //alreadyExists = false;
                return false;
            }
        }

        // add new node to the list of connected nodes and return the list of connected nodes
        public string addNode(string newNodeAddress)
        {
            // create the new node instance and add it to the network
            Console.WriteLine("adding the new node " + newNodeAddress + " to my list of my nodes");
            INodeContract newNode = createServerNode(newNodeAddress);
            if (!connectedNodes.ContainsKey(newNodeAddress))
                connectedNodes[newNodeAddress] = newNode;
            connectedNodes[NodeAddress] = null;
            propagateNewNode(newNodeAddress);
            // if the instance of this node is not yet in the network, add it
            string[] connectedNodesArr = new string[connectedNodes.Count];
            connectedNodes.Keys.CopyTo(connectedNodesArr, 0);
            string concatenatedIps = concatenateIps(connectedNodesArr);
            return concatenatedIps;
        }

        private string concatenateIps(string[] connectedNodesArr)
        {
            string ips = "";
            foreach (string curIp in connectedNodesArr)
            {
                ips += curIp + ",";
            }
            return ips.Substring(0, ips.Length - 1);
        }


        // propagate the new node to the other connected nodes, simultaneously also add each node
        // to which the new node has been propagated to the new node itself too.
        private void propagateNewNode(string newNodeAddress)
        {
            foreach (KeyValuePair<string, INodeContract> curNode in connectedNodes)
            {
                if (curNode.Key.Equals(NodeAddress) || curNode.Key.Equals(newNodeAddress))
                    continue;
                curNode.Value.addNode(newNodeAddress);
            }
        }
        // add this node to the list of the connected nodes (used to add the list of connected nodes to the
        // new node so that a newly connected node knows about the other nodes in the network.
        /*public bool addNodeWithoutPropagate(string nodeAddress)
        {
            INodeContract newNode = createServerNode(nodeAddress);
            connectedNodes[nodeAddress] = newNode;
            return true;
        }*/
        // sign off this node from the network by connecting the other nodes and removing its own
        // address from the list of the connected nodes
        public bool signOff()
        {
            //string myNodeUrl = formatUrl(NodeAddress);
            foreach (KeyValuePair<string, INodeContract> node in connectedNodes)
            {
                if (node.Key.Equals(NodeAddress))
                    continue;
                // disconnect all the nodes connected to the node
                node.Value.disconnectNode(NodeAddress);
            }
            return true;
        }

        // remove the node whose address is nodeAddress.
        public bool disconnectNode(string nodeAddress)
        {
            Console.WriteLine("Disconnecting the node " + NodeAddress);
            connectedNodes.Remove(nodeAddress);
            return true;
        }

        // invoked by the remote node when it wants to start the calculation
        public virtual bool calculateValue(int initialValue)
        {
            Console.WriteLine("Starting calculation with the initial value:" + initialValue);
            CurrentValue = initialValue;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            (new Thread(() =>
            {
                Console.WriteLine("Starting count towards " + totalProcessTime + "[sec]");
                while (stopWatch.ElapsedMilliseconds < totalProcessTime)
                {
                    int randomSecNum = generateRandomNum(20);
                    System.Threading.Thread.Sleep(randomSecNum * 1000);
                    // generate a new random operation and value and enqueue it to the oper.s queue
                    Console.WriteLine("Generating a random operation and operand and adding it to the operations queue");
                    OperationValuePair newOperValue = generateRndOper();
                    operQueue.Enqueue(newOperValue);
                    syncCalculation();
                }
            })).Start();
            return true;
        }

        protected abstract void syncCalculation();

        // this server node received a start calculation request from the command line with an initial value
        // it will propagate the start message to the other nodes in the network
        public virtual bool startCalculation(int initialValue)
        {
            Console.WriteLine("User started calculation on this machine, propagating start to the other nodes");
            (new Thread(() =>
            {
                propagateStartCalculation(initialValue);
            })).Start();
            return true;
            //int initialValue = generateRandomNum(1000);
        }
        // the node that received the start calculation message from the command line will propagate
        // the message to the other connected nodes.
        public void propagateStartCalculation(int initialValue)
        {
            foreach (KeyValuePair<string, INodeContract> curNode in connectedNodes)
            {
                if (curNode.Key.Equals(NodeAddress))
                    continue;
                try
                {
                    curNode.Value.calculateValue(initialValue);
                }
                catch (Exception)
                {
                    Console.Error.WriteLine("Exception occurred while propagating start calculation command");
                }
            }
        }
        // this method is responsible for running the calculations on the node
        // it uses the agrawala synchronization algorithm to make sure it can modify the value
        // i.e. no other nodes are currently modifying this value


        // perform a random arithmetic calculation on the node (this is the critical section and the node
        // is currently allowed to do so), at this time, other nodes will not be able to perform their
        // own modification on this value

        protected virtual void performCalculation(OperationValuePair operValPair)
        {
            int num = operValPair.RandomValue;

            // generate a random number to perform a random arithmetic calculation with the current value.
            switch (operValPair.Operation)
            {
                case 0:
                    CurrentValue += num;
                    break;
                case 1:
                    CurrentValue -= num;
                    break;
                case 2:
                    CurrentValue *= num;
                    break;
                case 3:
                    CurrentValue /= num;
                    break;
            }
        }
        // propagate the newly calculated value of this node to the other nodes on the network
        protected void propagateRecentOperation(OperationValuePair operValPair)
        {
            foreach (KeyValuePair<string, INodeContract> curNode in connectedNodes)
            {
                if (curNode.Key.Equals(NodeAddress))
                    continue;
                try
                {
                    curNode.Value.performOperation(operValPair.Operation, operValPair.RandomValue, 1);
                }
                catch (Exception)
                {
                    Console.Error.WriteLine("Exception occurred while propagating start calculation command");
                }
            }
        }
        // set the current value of this node to calcValue as received from the node that just finished
        // calculating this value
        public bool setCalcValue(int calcValue)
        {
            CurrentValue = calcValue;
            return true;
        }

        protected int generateRandomNum(int limit)
        {
            Random rnd = new Random();
            int num = rnd.Next(1, limit);
            return num;
        }

        protected string formatUrl(string ip)
        {
            return "http://" + ip + "/pds/";
        }

        private String getMyIp()
        {
            String myIp = "";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            //get the local IP address
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    myIp = ip.ToString();
                }
            }
            //return myIp;
            return "127.0.0.1:8080";
            //return "192.168.0.100:8081";
        }

        public bool setMyIp(string ip)
        {
            UnformattedNodeAddress = ip;
            NodeAddress = formatUrl(UnformattedNodeAddress);
            return true;
        }

        // generate a random operation and operand
        protected OperationValuePair generateRndOper()
        {
            int oper = generateRandomNum(4);
            // generate a random number to perform a random arithmetic calculation with the current value.
            int num = generateRandomNum(10);

            OperationValuePair operValuePair = new OperationValuePair(oper, num);
            return operValuePair;
        }

        // create a server node using its url

        protected abstract INodeContract createServerNode(string url);
        protected abstract void createSyncAlgorithm();


        public class OperationValuePair
        {
            public OperationValuePair(int operation, int rndVal)
            {
                this.Operation = operation;
                this.RandomValue = rndVal;
            }
            public int Operation { get; set; }
            public int RandomValue { get; set; }
        }

        // perform remote node operation
        public virtual bool performOperation(int operation, int operand, int p)
        {
            Console.WriteLine("Received operation from remote node");
            Console.WriteLine("Performing this calculation: " + printOperation(new OperationValuePair(operation, operand)));
            switch (operation)
            {
                case 0:
                    CurrentValue += operand;
                    break;
                case 1:
                    CurrentValue -= operand;
                    break;
                case 2:
                    CurrentValue *= operand;
                    break;
                case 3:
                    CurrentValue /= operand;
                    break;
            }
            Console.WriteLine("The result of recent the most operation received is:" + CurrentValue);
            return true;
        }

        protected string printOperation(OperationValuePair operValPair)
        {
            string oper = operValPair.Operation == 0 ? "+" : operValPair.Operation == 1 ? "-" : operValPair.Operation == 2 ? "*" : "/";
            return CurrentValue + oper + operValPair.RandomValue;
        }

    }
}