﻿using Microsoft.Samples.XmlRpc;
using Agrawala;
using PDSService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Agrawala
{
    // This is the implementation of the interface provided for the clients for the Agrawala
    // synchronization algorithm, this needs to reside only on the server side and is accessed 
    // by a client/remote nodes using rpc.
    public class ServerNodeA : ServerNode, INodeContractA
    {
       
        private AgrawalaSync agrawala;

        public ServerNodeA() { }

        // received ok from another node in the network, see if the number of okay received equals
        // the number of the connected on the network and accordingly decide to enter the 
        //critial section or not
        public bool receiveOk(int senderTimestamp)
        {
            agrawala.receiveOk(senderTimestamp);
            if (agrawala.canUseResource())
            {
                calcAndRelease();
            }

            return true;
        }
        
        protected override void syncCalculation()
        {
            agrawala.sendRequest(connectedNodes);
            if (agrawala.canUseResource())
            {
                calcAndRelease();
            }
            else
            {
                //Monitor.Wait(this);
            }

        }

        private void calcAndRelease()
        {
            performCalculation(operQueue.Dequeue());
            // propagate this operation and value
            //propagateNewValue();
            string nextNodeUrl = agrawala.releaseResource();
            INodeContractA nextNode = (INodeContractA)connectedNodes[nextNodeUrl];
            nextNode.receiveOk(agrawala.Timestamp);
        }
      

        // create a server node using its url
        protected override INodeContract createServerNode(string url)
        {
            //url = "http://" + url +"/pds/";
            ChannelFactory<INodeContractA> cf = new ChannelFactory<INodeContractA>(new WebHttpBinding(), url);
            cf.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());
            INodeContractA node = cf.CreateChannel();
            return node;
        }
        
        // the node now received a request from requesterId with the requester time stamp, 
        // return true if this node can send okay, and enqueue the request and return false otherwise
        public bool receiveRequest(string requesterId, int requesterTimestamp)
        {
            return agrawala.receiveRequest(requesterId, requesterTimestamp);
        }
        // return the timestamp of this node
        public int getTimestamp()
        {
            return agrawala.Timestamp;
        }

        protected override void createSyncAlgorithm()
        {
            agrawala = new AgrawalaSync(NodeAddress, connectedNodes.Count);
        }
    }
}

