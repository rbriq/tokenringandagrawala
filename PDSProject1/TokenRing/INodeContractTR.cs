﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Microsoft.Samples.XmlRpc;
using PDSService;

namespace TokenRing
{
    [ServiceContract(Namespace = "PDSProject")]
    // This is the interface which the client/nodes use to send requests to the other nodes on the network.
    public interface INodeContractTR : INodeContract
    {

        [OperationContract(Action = "PDSProject.receiveToken")]
        bool receiveToken();

        [OperationContract(Action = "PDSProject.done")]
        bool done(string url);
    }
}
