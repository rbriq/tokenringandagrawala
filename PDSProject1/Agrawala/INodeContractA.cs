﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Microsoft.Samples.XmlRpc;
using PDSService;

namespace Agrawala
{
    [ServiceContract(Namespace = "PDSProject")]
    // This is the interface which the client/nodes use to send requests to the other nodes on the network.
    public interface INodeContractA : INodeContract
    {
        [OperationContract(Action = "receiveRequest")]
        bool receiveRequest(string nodeId, int requesterTimestamp);

        [OperationContract(Action = "getTimestamp")]
        int getTimestamp();

        [OperationContract(Action = "receiveOk")]
        bool receiveOk(int requesterTimestamp);

    }
}
