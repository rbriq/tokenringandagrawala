﻿using Microsoft.Samples.XmlRpc;
using Agrawala;
using PDSService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace TokenRing
{
    // This is the implementation of the interface provided for the clients for the Token Ring
    // synchronization algorithm, this needs to reside only on the server side and is accessed 
    // by a client/remote nodes using rpc.
    public class ServerNodeTR : ServerNode, INodeContractTR
    {

        private TokenRingSync tokenRingSync;
        private int nodeOrder;
        // the next node to which the token should be passed from this node
        private INodeContractTR nextNode;
        //indicates whether the 20 seconds time is over
        private bool timeOver;
        // indicates whether all operations in the queue are finished
        private bool IAmDone = false;
        // indicates whether all other operations are done
        private bool AllOtherDone = false;

        private bool unpropagatedDone = true;

        // the initial number of nodes
        private int initialSize = 0;

        private readonly object syncLock = new object();

        protected SortedList<string, INodeContractTR> removedNodes = new SortedList<string, INodeContractTR>();
        //private IList <string> orderedNetNodes = new List<string>();
        public ServerNodeTR()
            : base()
        {
        }

        public override bool join(string url)
        {
            Console.WriteLine("The node on this host wants to join a node on the network with ip:" + url);
            string existingNodeUrl = formatUrl(url);
            INodeContract existingNetworkNode = createServerNode(url);

            //connectedNodes[url] = existingNetworkNode;
            try
            {
                string[] returnedNodes = existingNetworkNode.addNode(NodeAddress).Split(',');
                foreach (string curNode in returnedNodes)
                {
                    connectedNodes[curNode] = createServerNode(curNode);
                    // orderedNetNodes.Add(curNode);
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Exception occurred while joining the network: " + e.Message);
                //alreadyExists = false;
                return false;
            }
            nodeOrder = connectedNodes.Count;
            return true;
        }



        // this server node received a start calculation request from the command line with an initial value
        // it will propagate the start message to the other nodes in the network
        public override bool startCalculation(int initialValue)
        {
            // assign token to the first node
            //if(NodeAddress == connectedNodes[0])
            //connectedNodes[0].
            initialSize = connectedNodes.Count;
            setNextNodeAddress();
            base.startCalculation(initialValue);
            Console.WriteLine("Starting my own calculation");
            calculateValue(initialValue);
            return true;
        }

        private void resetValues()
        {
            tokenRingSync.OwnsToken = true;
            IAmDone = false;
            AllOtherDone = false;
            tokenRingSync.AllOtherDone = false;
            timeOver = false;
            unpropagatedDone = true;
        }

        // create a server node using its url
        protected override INodeContract createServerNode(string url)
        {
            ChannelFactory<INodeContractTR> cf = new ChannelFactory<INodeContractTR>(new WebHttpBinding(), url);
            cf.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());
            INodeContractTR node = cf.CreateChannel();
            return node;
        }
        // the main function for running and synchronizing the calculations accross the nodes
        public override bool calculateValue(int initialValue)
        {
            resetValues();
            if (tokenRingSync.NextNode == null)
                setNextNodeAddress();
            Console.WriteLine("Starting calculation with the initial value:" + initialValue);
            CurrentValue = initialValue;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            new Thread(() =>
            {
                Console.WriteLine("Starting count towards " + totalProcessTime + "[sec]");
                while (stopWatch.ElapsedMilliseconds < totalProcessTime)
                {
                    int randomSecNum = generateRandomNum(6);
                    if (tokenRingSync.OwnsToken)
                    {
                        tokenRingSync.passToken();
                    }
                    Console.WriteLine("Going to sleep for " + randomSecNum + "[sec]");
                    System.Threading.Thread.Sleep(randomSecNum * 1000);
                    OperationValuePair newOperValue = generateRndOper();
                    operQueue.Enqueue(newOperValue);
                    syncCalculation();
                }
                timeOver = true;
            }).Start();
            //emptyQueue();
            return true;
        }
        // I woke up after sleeping and would like to perform the calculation, but first I need to sync
        // with the other nodes and check if i can enter the critical section
        protected override void syncCalculation()
        {
            tokenRingSync.NeedResource = true;
            lock (syncLock)
            {
                if (tokenRingSync.OwnsToken)
                {
                    // perform my own calculation
                    if (operQueue.Count > 0 && tokenRingSync.OwnsToken)
                        performCalculation(operQueue.Dequeue());
                    else if (!timeOver)
                    {
                        tokenRingSync.passToken();
                    }
                    else if (timeOver && operQueue.Count < 1 && unpropagatedDone)
                    {
                        IAmDone = true;
                        propagateDone();
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }

        protected override void performCalculation(OperationValuePair operValPair)
        {
            Console.WriteLine("Critical section start: performing the calculation: " + printOperation(operValPair));
            base.performCalculation(operValPair);
            Console.WriteLine("The result of the operation is:" + CurrentValue);
            propagateRecentOperation(operValPair);
            tokenRingSync.passToken();
            Console.WriteLine("Critical section end: result:" + CurrentValue);
        }

        public bool receiveToken()
        {
            //Console.WriteLine("received Token");
            (new Thread(() =>
            {
                tokenRingSync.receiveToken();
                if ((tokenRingSync.NeedResource || timeOver) && operQueue.Count > 0)
                {
                    if (operQueue.Count > 0 && tokenRingSync.OwnsToken)
                        performCalculation(operQueue.Dequeue());
                }
                else if (timeOver && (operQueue.Count < 1 || IAmDone) && unpropagatedDone)
                {
                    propagateDone();
                }
            })).Start();

            return true;
        }

        private void propagateDone()
        {
            Console.WriteLine("propagating done");
            unpropagatedDone = false;
            foreach (KeyValuePair<string, INodeContract> curNode in connectedNodes)
            {
                if (curNode.Key.Equals(NodeAddress))
                    continue;
                ((INodeContractTR)(curNode.Value)).done(UnformattedNodeAddress);
            }
        }


        protected void setNextNodeAddress()
        {
            if (connectedNodes.Count < 2)
            {
                tokenRingSync.NextNode = null;
                return;
            }
            int nextI = (connectedNodes.IndexOfKey(NodeAddress) + 1) % connectedNodes.Count;
            string nextNodeAddress = connectedNodes.ElementAt(nextI).Key;
            nextNode = (INodeContractTR)connectedNodes.ElementAt(nextI).Value; //  extract the node next in the map
            tokenRingSync.NextNodeAddress = nextNodeAddress;
            tokenRingSync.NextNode = nextNode;
        }

        protected override void createSyncAlgorithm()
        {
            tokenRingSync = new TokenRingSync(NodeAddress);
        }

        public bool done(string doneNodeIp)
        {
            if (!doneNodeIp.StartsWith("http"))
                doneNodeIp = formatUrl(doneNodeIp);

            removedNodes[doneNodeIp] = (INodeContractTR)connectedNodes[doneNodeIp];
            if (removedNodes.Count == initialSize)
            {
                AllOtherDone = true;
                tokenRingSync.AllOtherDone = true;
            }
            return true;
        }
    }
}

