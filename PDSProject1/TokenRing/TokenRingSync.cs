﻿using PDSService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TokenRing
{
    class TokenRingSync
    {
        // the address of the node
        public string NodeId { get; set; }
        // indicates whether this node wants to use the shared resource
        public bool NeedResource { get; set; }
        public bool AllOtherDone { get; set; }
        public INodeContractTR NextNode { set; get; }

        public bool OwnsToken { get; set; }

        //public bool AllOtherDone { get; set; }
        //public bool IAmDone { get; set; }

        public string NextNodeAddress { get; set; }

        public TokenRingSync(string nodeId)
        {
            NodeId = nodeId;
            OwnsToken = false;
        }

        public void passToken()
        {
            NeedResource = false;
            OwnsToken = false;
            //Console.WriteLine("Passing token to node " + NextNodeAddress);
            if (AllOtherDone)
                return;
            try
            {
                NextNode.receiveToken();
            }
            catch (Exception)
            {
                Console.Error.WriteLine("Exception occurred while invoking receiveToken of remote node");               
            }
        }

        public void receiveToken()
        {
            //Console.WriteLine("Received token");
            OwnsToken = true;
            
            if (!NeedResource)
            {
                //Console.WriteLine(" but I don't need it, I will pass it on straight on");
                passToken();
            }

        }

    }
}
