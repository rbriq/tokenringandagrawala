﻿using PDSService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
namespace Agrawala
{
    class AgrawalaSync
    {
        //  the queue of the requests received at this node
        private Queue <string> requestsQueue;
        // the node address of this 
        public string NodeId { get; set; }
        // the time stamp value currently on this node
        public int Timestamp { get; set; }
        // indicates whether this node wants to use the shared resource
        public bool WantToUseResource { get; set; }
        // indicates whether this resource is being used by this node
        public bool AmIUsingResource { get; set; }
        // the number of nodes in the network
        private int NodesNumber { get; set; }
        // the number of ok.s this node received upon sending a request to enter a critical section
        private int okCount = 0;

        public AgrawalaSync(string nodeId, int nodesNumber)
        {
            requestsQueue = new Queue<string>();
            Timestamp = 0;
            NodeId = nodeId;
            NodesNumber = nodesNumber;
        }

        public void enqueueRequest(string requesterId)
        {
            requestsQueue.Enqueue(requesterId);
        }

        public void removeRequest()
        {
            requestsQueue.Dequeue();
        }
        // the node now received a request from requesterId with the requester time stamp, 
        // return true if this node can send okay,  enqueue the request and return false otherwise
        public bool receiveRequest(string requesterId, int requesterTimeStamp)
        {
            if (!AmIUsingResource && !WantToUseResource)
            {
                determineTimeStamp(requesterTimeStamp);
                return true;
            }
            else if (AmIUsingResource)
            {
                requestsQueue.Enqueue(requesterId);
                return false;
            }
            else if (WantToUseResource)
            {
                if (requesterTimeStamp < Timestamp)
                {
                    ++Timestamp;
                    return true;
                }
                else
                {
                    Timestamp = requesterTimeStamp + 1;
                    requestsQueue.Enqueue(requesterId);
                    return false;
                }
            }
            return true;
        }
        // release the resource and return the node address of the top of the queue.
        public string releaseResource()
        {
            AmIUsingResource = false;
            WantToUseResource = false;
            okCount = 0;
            string waitingRequester = requestsQueue.Dequeue();
            return waitingRequester;
        }
        // this node would like to use the request and sends a request to the other connected nodes 
        // in the network
        public void sendRequest(SortedList<string, INodeContract> connectedNodes)
        {
            // this is a new request, initialise the okay.s number received to 0
            okCount = 0;
            WantToUseResource = true;
            ++Timestamp;
            // poll all the other nodes by invoking their receiveRequest method
            foreach (KeyValuePair<string, INodeContract> curNode in connectedNodes)
            {
                INodeContractA curNodeValueA = (INodeContractA)curNode.Value;
                if (curNodeValueA.receiveRequest(NodeId, Timestamp))
                    receiveOk(curNodeValueA.getTimestamp());
            }
            NodesNumber = connectedNodes.Count;
        }
        // this node received ok upon sending a request and polling the other nodes about the shared resource
        public void receiveOk(int senderTimestamp)
        {
            determineTimeStamp(senderTimestamp);
            ++okCount;
        }
        // the logic for increasing the time stamp by the lamport clock algorithm
        private void determineTimeStamp(int senderTimestamp)
        {
            Timestamp = senderTimestamp > Timestamp ? senderTimestamp + 1 : Timestamp + 1;
        }
        // returns whether thos node can enter the critical section: returns ok when the numbers of
        // okays received equals the number of the nodes in the network -1.
        public bool canUseResource()
        {
            return okCount == (NodesNumber-1);
        }

        public void incrementTimestamp()
        {
            ++Timestamp;
        }
    }

    
}
