﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Microsoft.Samples.XmlRpc;

namespace PDSService
{
    [ServiceContract(Namespace = "PDSProject")]
    // This is the base interface which the client/nodes use to send requests to the other nodes on the network.
    // both Agrawala and token ring will extend this interface.
   
    public interface INodeContract
    {
        // method exposed to the Commandline
        [OperationContract(Action = "join")]
        bool join(string nodeAddress);

        // method exposed to other nodes
        [OperationContract(Action = "PDSProject.addNode")]
        string addNode(string newNodeAddress);


        [OperationContract(Action = "PDSProject.disconnectNode")]
        bool disconnectNode(string nodeAddress);
        // method exposed to the command line, it will call calculateValue of the other nodes 
        // to start their calculation.
        [OperationContract(Action = "PDSProject.startCalculation")]
        bool startCalculation(int initialValue);

        // method exposed to the commandline, it will call disconnect node to sign itself off from the rest 
        // of the network
        [OperationContract(Action = "PDSProject.signOff")]
        bool signOff();

        // invoked by the remote node when it wants to start the calculation
        [OperationContract(Action = "PDSProject.calculateValue")]
        bool calculateValue(int initialValue);

       
        // invoked by remote node when it wants to propagate its operation
        [OperationContract(Action = "PDSProject.performOperation")]
        bool performOperation(int operation, int operand, int requesterTimestamp);

        // set my ip as per the one entered in the command line
        [OperationContract(Action = "PDSProject.setMyIp")]
        bool setMyIp(string ip);
    }
}
