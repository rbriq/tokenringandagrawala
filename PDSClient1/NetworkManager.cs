﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Samples.XmlRpc;
using System.ServiceModel;
using System.Net;
using PDSService;

namespace PDSServiceHost
{
    public enum SynchAlgorithm { TokenRing, RicartAgrawala };

    // a helper class used by the client to create an instance of a new node, remotely
    // invoking its methods and connecting it to the network.
    class NetworkManager
    {
        private SynchAlgorithm synchAlgorithm;
        // the server node of this host
        public INodeContract serverNode { get; set; }
        public NetworkManager()
        {
            // the url of the the node that wants to join the network is the same as the address it is 
            // being joined from
            string url = "http://localhost:8000/pds/";
            serverNode = createServerNode(url);
        }

        private INodeContract createServerNode(string url)
        {
            ChannelFactory<INodeContract> cf = new ChannelFactory<INodeContract>(new WebHttpBinding(), url);
            cf.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());
            INodeContract node = cf.CreateChannel();
            return node;
        }

        // The node running on this server wishes to join the network, nodeAddress is the address of one
        // of the nodes in network, which in turn will return the list of the nodes on the network
        public bool join(String nodeAddress)
        {
            string url = formatUrl(nodeAddress);
            try
            {
                return serverNode.join(url);
            }
            catch (Exception)
            {
                Console.Error.WriteLine("Exception occurred while joining the node, the node could not be added to the network");
                return false;
            }
        }

        public void signoff()
        {
            try
            {
                serverNode.signOff();
            }
            catch (Exception)
            {
                Console.Error.WriteLine("Exception occurred while asigning off");
            }

        }


        public bool start(int initValue)
        {
            try
            {
                serverNode.startCalculation(initValue);
                return true;
            }
            catch (Exception)
            {
                Console.Error.WriteLine("Exception occurred while starting the calculation");
                return false;
            }


        }

        public static bool validateIP(string ip)
        {
            /*IPAddress clientIpAddr;
            if (ip.Split('.').Length == 4)
            {
                return IPAddress.TryParse(ip, out clientIpAddr);
            }
            return false;*/
            return true;
        }

        private string formatUrl(string nodeAddress)
        {
            string url = "http://" + nodeAddress + "/pds/";
            return url;
        }

        internal void setSyncAlgorithm(int synchAlgorithm)
        {
            this.synchAlgorithm = (SynchAlgorithm)synchAlgorithm;
        }

        internal void setHostIp(string hostIp)
        {
            serverNode.setMyIp(hostIp);
        }
    }
}
