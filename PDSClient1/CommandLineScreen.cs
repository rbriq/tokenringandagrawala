﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDSServiceHost
{
    // This is the class ran on the client side which the users uses to type in his commands.
    class CommandLineScreen
    {
        static void Main(string[] args)
        {
            NetworkManager networkmanager = new NetworkManager();
            string line;
            /*
            System.Console.WriteLine("Please specify the synchronization algorithm that you would like to use: 0 for Token Ring, 1 for Ricart & Agrawala");
            while ((line = Console.ReadLine()) != null)
            {
                if (line.Equals("0") || line.Equals("1"))
                {
                    networkmanager.setSyncAlgorithm(int.Parse(line));
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid parameter, please enter 0 or 1");
                    continue;
                }
            }*/
            //System.Console.WriteLine("Please enter your ip and port in the format ip:port");
            //string hostIp = Console.ReadLine();
            //networkmanager.setHostIp(hostIp);
            System.Console.WriteLine("Please type a network operation(join/signoff/start) or exit");
            while ((line = Console.ReadLine()) != null)
            {
                string[] operAdddress = line.Split(' ');
                switch (operAdddress[0])
                {
                    case "join":
                       /*string[] ipPort;
                        if (operAdddress.Length < 2 || (ipPort = operAdddress[1].Split(':')).Length < 2)
                        {
                            Console.WriteLine("Missing parameters for the join command");
                            continue;
                        }
                        if (!NetworkManager.validateIP(ipPort[0]))
                        {
                            Console.WriteLine("The IP address specified is invalid");
                            continue;
                        }
                         if (!networkmanager.join(ipPort[0]))
                           Console.WriteLine("This IP address does not exist in the network");
                        //networkmanager.join(operAdddress[1]);*/
                        networkmanager.join("127.0.0.1:8000");
                        //networkmanager.join("172.16.1.100:8080");
                        break;
                    case "signoff":
                        networkmanager.signoff();
                        break;
                    case "start":
                        int initialValue;
                        if (operAdddress.Length < 2 || !int.TryParse(operAdddress[1], out initialValue))
                        {
                            Console.WriteLine("Missing parameters for the join command");
                            continue;
                        }
                        //int initialValue = int.Parse(operAdddress[1]);
                        networkmanager.start(initialValue);
                        break;
                    case "exit":
                        return;
                    default:
                        Console.WriteLine("invalid operation, please enter a valid operation ");
                        break;
                }
            }
        }
    }
}
