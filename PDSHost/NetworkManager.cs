﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Samples.XmlRpc;
using System.ServiceModel;
using PDSService;

namespace ServiceHost
{
    class NetworkManager
    {
        private IDictionary<string, INodeContract> connectedNodes;
        private ChannelFactory<INodeContract> cf;

        public NetworkManager()
        {
            cf = new ChannelFactory<INodeContract>(new WebHttpBinding(), "http://www.example.com/xmlrpc");

            cf.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

            connectedNodes = new Dictionary<string, INodeContract>();
        }
    }
}
